class Article::Filter

  def filter(scope, query_params)

    if query_params[:category].present?
      scope = scope.where(category: query_params[:category])
    end

    scope
  end

end