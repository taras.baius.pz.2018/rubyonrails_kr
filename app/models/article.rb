class Article < ApplicationRecord
    include Visible
    has_many :comments, dependent: :destroy

    validates :title, presence:true
    validates :body, presence: true, length: {minimum: 10}

    scope :filtered, ->(query_params) { Article::Filter.new.filter(self, query_params) }
end
